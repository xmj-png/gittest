#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @author: xiaoming
# @file: flask_service.py
# @time: 2024/4/15 16:17
# @desc:

# -*- coding: utf-8 -*-

from flask import Flask,request,jsonify


app = Flask(__name__)


@app.route('/api/login',methods=['POST'])
def login():

    data = request.get_json()

    username = data['username']
    passwd = data['password']
    if username=='' or passwd=='':
        return jsonify({
            "code":"001",
            "msg":"用户名和密码不能为空"
        })
    elif username == 'zz' and passwd == '123456':
        return jsonify({
            "adress": {
                "city": "北京"
            },
            "httpstatus": 200,
            "info": {
                "age": 22,
                "name": "zhx"
            },
            "mag": "success",
            "token": " 1HJHSJSNKSKSKSNSKHSKSDUIU"
        })
    else:
        return jsonify({
            "code": "001",
            "msg": "username or password is wrong"
        })



if __name__ == '__main__':
    app.run(host='127.0.0.1',port='8080')