from time import sleep
import pytest
import asyncio



def async_function(i):
    print("开始计时")
    sleep(i)
    return "ss"

@pytest.mark.asyncio
async def test_async_function():
    result = await async_function(5)
    assert result == "ssw"


if __name__ == '__main__':
    pytest.main()