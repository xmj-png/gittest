import time
import tidevice
from tidevice._perf import DataType

t = tidevice.Device()
# perf = tidevice.Performance(t,[DataType.CPU, DataType.MEMORY, DataType.NETWORK, DataType.FPS, DataType.PAGE, DataType.SCREENSHOT, DataType.GPU])
perf = tidevice.Performance(t, DataType.MEMORY)
perf1 = tidevice.Performance(t, DataType.FPS)


def callback(_type: tidevice.DataType, value: dict):
    print(_type.value, value)


perf.start('com.magicv.AirBrush', callback=callback)
perf1.start('com.magicv.AirBrush', callback=callback)
time.sleep(60)
perf.stop()
perf1.stop()
